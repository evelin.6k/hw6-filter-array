
function filterBy (array, type){
    let newArray = array.filter(function(element){
    return typeof (element) !== type;
    })
    if(type === 'object'){
        newArray.push(null);
    }
    return newArray;
}

console.log( filterBy(['hello', 'world', 23, '23', null], 'string'));
